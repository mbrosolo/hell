#ifndef	__EDGE_H__
#define __EDGE_H__

class Edge
{
	char	_c;
public:
	Edge();
	Edge(const char);
	Edge(Edge const &);
	Edge & operator=(Edge const &);
	~Edge();

	bool operator()(char);
	bool operator==(Edge const &);
};

#endif