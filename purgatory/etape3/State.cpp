#include <sstream>
#include "State.h"


State::State(const std::string &name)
	: _name(name)
{
}

bool	State::operator==(State const &that)
{
	return !this->_name.compare(that._name);
}

const std::string & State::getName(void) const
{
	return (this->_name);
}

State * State::create(void)
{
	static int	nb = 0;
	std::stringstream ss;
	std::string s;

	ss << "S" << nb;
	ss >> s;
	return new State(s);
}

void State::setFinal(bool final)
{
	this->_final = final;
}

bool State::getFinal() const
{
	return this->_final;
}

void State::link(Edge const &e, std::string const &stateName)
{
	Edge *_e = new Edge(e);
	this->_states[_e] = stateName;
}

const	std::string	     State::find(char c) {
	std::map<Edge *, std::string>::iterator	it;
	for(it = this->_states.begin(); it != this->_states.end(); it++) {
		if ((*it).first->operator()(c)) {
			return (*it).second;
		}
	}
	return "";
}
