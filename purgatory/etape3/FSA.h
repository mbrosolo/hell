#ifndef	__FSA_H__
#define	__FSA_H__

#include <map>
#include "Edge.h"
#include "State.h"

class FSA
{
	std::map<const std::string, State *>	_states;
	std::string								_initial;

public:
	void	add(State *);
	State	*operator[](const std::string &);
	void	setInitial(State *);
	void	setInitial(const std::string &);
};

#endif