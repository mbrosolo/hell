#include "FSA.h"

void	FSA::add(State *etat) {
	this->_states[etat->getName()] = etat;
}

State	*FSA::operator[](const std::string &state_name) {
	std::map<const std::string, State *>::iterator	it;
	it = this->_states.find(state_name);
	if (it == this->_states.end())
		return NULL;
	return (*it).second;
}

void	FSA::setInitial(State *etat) {
	this->_initial = etat->getName();
}

void	FSA::setInitial(const std::string &state_name) {
	this->_initial = state_name;
}