#include "Edge.h"


Edge::Edge(const char c) {
	this->_c = c;
}

Edge::Edge(Edge const &that)
{
	this->_c = that._c;
}

Edge::~Edge()
{
}

bool Edge::operator()(char c) {
	return this->_c == c;
}

bool Edge::operator==(Edge const &that)
{
	return this->_c == that._c;
}