#ifndef	__STATE_H__
#define __STATE_H__

#include <map>
#include <string>
#include "Edge.h"

class State
{
	std::string _name;
	bool _final;
	std::map<Edge *, std::string> _states;

public:
	State();
	State(const std::string &);
	State(State const &);
	State & operator=(State const &);
	~State();

	bool			operator==(State const &);
	const			std::string &	getName(void) const;
	static State * create(void);
	void setFinal(bool const final);
	bool getFinal() const;
	void link(Edge const &, std::string const &);
	const std::string find(const char);
};

#endif
