#ifndef __MATCHER_H__
#define	__MATCHER_H__

#include	"FSA.h"

class Matcher
{
	FSA	&_fsa;

public:
	Matcher(FSA &);

	bool	find(const std::string &);
};

#endif