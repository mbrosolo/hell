/* 
 * File:   main.cpp
 * Author: MaTT
 *
 * Created on 12 décembre 2012, 17:55
 */

#include <iostream>
#include <string>
#include <stdlib.h>
#include "Machine.hpp"

extern enum eState gStateTable[STATE_MAX][EDGE_MAX];
extern enum eAction gActionTable[STATE_MAX][EDGE_MAX];

int getAlph(char c, std::string const & s)
{
	int i = 0;

	while (i < s.size())
	{
		if (c == s[i])
			break;
		++i;
	}
	return i;
}

int main(int argc, char **argv)
{
	std::string s("mechant");

	if (argc < 2)
		return -1;
	//std::cout << "Begin." << std::endl;
	int i = 0;
	int _i = 0;
	std::string input(argv[1]);
	enum eState currentState = S0;
	enum eState nextState = S0;
	enum eAction action = MA;
	int current;
	while (i < input.size())
	{
		//std::cout << input[i] << std::endl;
		current = getAlph(input[i], s);
		if (currentState == S0)
			_i = i;
		nextState = gStateTable[currentState][current];
		action = gActionTable[currentState][current];
		//std::cout << currentState << "|" << nextState << "|" << action << std::endl;
		if (action == MA)
		{
			//std::cout << "MA" << std::endl;
			currentState = nextState;
			action = gActionTable[currentState][current];
			if (action == HR)
			{
				std::cout << s << std::endl;
				currentState = S0;
			}
		}
		else
		{
			//std::cout << "Error" << std::endl;
			i = _i;
			currentState = S0;
		}
		//std::cout << currentState << "|" << nextState << "|" << action << std::endl;		
		//std::cout << "action:" << action << std::endl;			
	++i;
	}
	//action = gActionTable[currentState][current];
	//if (action == HR)
	//	std::cout << s << std::endl;
	return 0;
}
