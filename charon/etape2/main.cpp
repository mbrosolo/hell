/* 
 * File:   main.cpp
 * Author: MaTT
 *
 * Created on 12 décembre 2012, 17:55
 */

#include <iostream>
#include <stdlib.h>
#include <boost/bind.hpp>
#include "Function.hpp"

int function(char c)
{
    std::cout << "Output:[" << c << "]" << std::endl;
	return (0);
}

int function2(char c)
{
    std::cout << "DATPUT:[" << c << "]" << std::endl;
	return (0);
}

int function3(char c)
{
    std::cout << "ANOTHERPUT:[" << c << "]" << std::endl;
	return (0);
}

int main(int argc, char **argv)
{
    Function<int (char)> f = &function;
    Function<int (char)> f2 = &function;
    
    f('c');
    f = &function2;
    f('d');
	f2 = f = boost::bind(&function3, _1);
	f('e');
	f2('f');
    return 0;
}