/* 
 * File:   FunctionSignature.hpp
 * Author: MaTT
 *
 * Created on 14 décembre 2012, 23:52
 */

#ifndef __FUNCTION_HPP__
#define	__FUNCTION_HPP__

template<typename T>
class Function
{
//	class ICallable
//	{
//		virtual ICallable * operator()(T) = 0;
//	};
//	template<typename U>
//	class Callable : public ICallable
//	{
//		public:
//			U _call;
//			Callable(U call) : _call(call) {};
//			virtual ICallable * operator()()
//			{
//				return f();
//			};
//	};
//
//	ICallable * _call;
//public:
//	Function(T t)
//		: _call(new Callable(t)) {}
//    Function & operator=(T t)
//    {
//        _call = new Callable(t);
//		return *this;
//    }
//    T operator()()
//	{
//		return this->_call();
//	}
};

template<typename T, typename P1>
class Function<T (P1)>
{
	class ICallable
	{
	public:
		virtual T operator()(P1) = 0;
	};
	template<typename U>
	class Callable : public ICallable
	{
		U _f;
	public:
		Callable(U f) : _f(f) {}
		virtual T operator()(P1 p1)
		{
			return _f(p1);
		}
	};

	ICallable * _call;
public:
	template<typename U>
	Function(U u)
		: _call(new Callable<U>(u)) {}
	template<typename U>
    Function & operator=(U u)
    {
        _call = new Callable<U>(u);
		return *this;
    }
    T operator()(P1 p1)
	{
		return this->_call->operator()(p1);
	}
};

#endif	/* __FUNCTION_HPP__ */

