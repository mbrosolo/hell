/* 
 * File:   main.cpp
 * Author: MaTT
 *
 * Created on 12 décembre 2012, 17:55
 */

#include <iostream>
#include <string>
#include <stdlib.h>
#include "FunctionSignature.hpp"

int oneFunction(std::string const & str)
{
    std::cout << "Output:[" << str << "]" << std::endl;
	return 0;
}

int twoFunction(std::string const & str1, std::string const & str2)
{
    std::cout << "Output:[" << str1 << "|" << str2 <<  "]" << std::endl;
	return 0;
}

int threeFunction(std::string const & str1, std::string const & str2, const std::string &str3)
{
    std::cout << "Output:[" << str1 << "|" << str2 << "|" << str3 << "]" << std::endl;
	return 0;
}

int fourFunction(std::string const & str1, std::string const & str2, const std::string &str3, const std::string &str4)
{
    std::cout << "Output:[" << str1 << "|" << str2 << "|" << str3 << "|" << str4 <<  "]" << std::endl;
	return 0;
}

int main(int argc, char **argv)
{
    FunctionSignature<int (const std::string& str)>::type f = &oneFunction;
    FunctionSignature<int (const std::string& str1, const std::string& str2)>::type f2 = &twoFunction;
    FunctionSignature<int (const std::string& str1, const std::string& str2, const std::string &str3)>::type f3 = &threeFunction;
    FunctionSignature<int (const std::string& str1, const std::string& str2, const std::string &str3, const std::string &str4)>::type f4 = &fourFunction;

    f("coucou");
    f2("c'est", "moi");
	f3("le", "petit", "codeur");
	f4("chinois", "beau", "et", "adroit");
    return 0;
}
