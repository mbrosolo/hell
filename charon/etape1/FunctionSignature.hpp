/* 
 * File:   FunctionSignature.hpp
 * Author: MaTT
 *
 * Created on 14 décembre 2012, 23:52
 */

#ifndef __FUNCTIONSIGNATURE_HPP__
#define	__FUNCTIONSIGNATURE_HPP__

template<typename T> struct FunctionSignature
{
    typedef T* type;
};

template<typename T, typename P1> struct FunctionSignature<T (P1)>
{
    typedef T (*type)(P1);
};

template<typename T, typename P1, typename P2> struct FunctionSignature<T (P1, P2)>
{
    typedef T (*type)(P1, P2);
};

template<typename T, typename P1, typename P2, typename P3> struct FunctionSignature<T (P1, P2, P3)>
{
    typedef T (*type)(P1, P2, P3);
};

template<typename T, typename P1, typename P2, typename P3, typename P4> struct FunctionSignature<T (P1, P2, P3, P4)>
{
    typedef T (*type)(P1, P2, P3, P4);
};
#endif	/* __FUNCTIONSIGNATURE_HPP__ */

